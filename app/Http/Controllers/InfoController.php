<?php

namespace App\Http\Controllers;

use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order_count = Order::whereDate('created_at', Carbon::today())->count();

        $order_count_current = Order::
        whereDate('created_at', Carbon::today())
            ->where(
            ['is_cancelled' => false,
            'is_completed' => false
            ])->count();

        $order_count_success = Order::
        whereDate('created_at', Carbon::today())
        ->where(
            ['is_completed' => true,
            'is_cancelled' => false
            ])->count();

        $order_count_cancelled = Order::
        whereDate('created_at', Carbon::today())
        ->where(
            ['is_cancelled' => true,
            'is_completed' => false
            ])->count();

        return response()->json(
            [
                'order_count' => $order_count,
                'order_count_current' => $order_count_current,
                'order_count_success' => $order_count_success,
                'order_count_cancelled' => $order_count_cancelled,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
