<?php
/**
 * Created by PhpStorm.
 * User: Edward
 * Date: 02.06.2019
 * Time: 23:19
 */

namespace App\Http\Helpers;


use App\Car;
use App\Driver;
use App\Place;
use App\User;

/**
 * Class Checker
 * Этот класс предназначен для проверок чего-либо
 * @package App\Http\Helpers
 */
class Checker
{
    public static function isValidUserObject($object)
    {
        return is_object($object) && $object instanceof User;
    }

    public static function isValidPlaceObject($object)
    {
        return is_object($object) && $object instanceof Place;
    }

    public static function isValidCarObject($object)
    {
        return is_object($object) && $object instanceof Car;
    }

    public static function isValidPriceForOrder($price)
    {
        return is_float($price) || is_integer($price);
    }

    public static function isValidPhoneForOrder($phone)
    {
        return is_string($phone);
    }

    public static function isAllowedToAssertCarForDriver(Car $car, Driver $driver)
    {
        // Чтобы разрешить назначать на автомобиль водителя, у водителя должен быть доступ к нему
        return $car->possible_drivers->contains($driver);
    }

    public static function isValidOrderTariff(int $tariff)
    {
        // Получаем список всех константант класса OrderTariff и смотрим по ключам, есть ли вхождение
        return in_array($tariff, array_values(OrderTariff::getAllTariffs()));
    }

    public static function isValidOrderStatus(int $status)
    {
        // Получаем список всех константант класса OrderStatus и смотрим по ключам, есть ли вхождение
        return in_array($status, array_values(OrderStatus::getAllStatuses()));
    }
}