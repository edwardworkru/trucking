<?php
/**
 * Created by PhpStorm.
 * User: Edward
 * Date: 02.06.2019
 * Time: 23:27
 */

namespace App\Http\Helpers;


use ReflectionClass;

class OrderStatus
{
    /**
     * @return array
     * @throws \ReflectionException
     */
    public static function getAllStatuses()
    {
        $oClass = new ReflectionClass(static::class);
        return $oClass->getConstants();
    }
}