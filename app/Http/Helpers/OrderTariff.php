<?php
/**
 * Created by PhpStorm.
 * User: Edward
 * Date: 16.05.2019
 * Time: 15:50
 */

namespace App\Http\Helpers;


use ReflectionClass;

class OrderTariff
{
    const CITY                    = 1; // В пределах города
    const OUT_OF_CITY             = 2; // За город
    const OUT_OF_CITY_AND_BACK    = 3; // За город и обратно

    /**
     * @return array
     * @throws \ReflectionException
     */
    public static function getAllTariffs()
    {
        $oClass = new ReflectionClass(static::class);
        return $oClass->getConstants();
    }
}