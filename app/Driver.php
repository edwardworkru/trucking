<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    /**
     * Cars that driver can use
     */
    public function owned_cars()
    {
        return $this->belongsToMany('App\Car', 'car_driver');
    }

    /**
     * Car that driver use just now
     */
    public function active_car()
    {
        return $this->belongsTo('App\Car', 'id', 'active_driver_id');
    }

    /**
     * Orders
     */
    public function orders()
    {
        return $this->hasMany('App\Order', 'driver_id', 'id');
    }

}
