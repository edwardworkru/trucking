<?php

namespace App;

use App\Http\Helpers\Checker;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use InvalidArgumentException;
use LogicException;

class Order extends Model
{

    public function complaints()
    {
        return $this->hasMany('App\Complaint');
    }

    public function car()
    {
        return $this->belongsTo('App\Car', 'car_id', 'id');
    }

    public function place_from()
    {
        return $this->belongsTo('App\Place', 'from_place_id', 'id');
    }

    public function place_to()
    {
        return $this->belongsTo('App\Place', 'to_place_id', 'id');
    }

    public function operator()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * Делаем заказ завершенным
     */
    public function assertCompleted()
    {
        $this->is_completed = true;
        $this->is_cancelled = false;

        $this->save();
    }

    /**
     * Делаем заказ отмененным
     */
    public function assertCancelled()
    {
        $this->is_completed = false;
        $this->is_cancelled = true;

        $this->save();
    }

    /**
     * Изменить оператора
     * @param $user
     */
    public function setOperator($user)
    {
        if (is_integer($user))
        {
            $user = User::findOrFail($user);
            $this->user_id = $user->id;
            $this->save();
        }
        else if (Checker::isValidUserObject($user))
        {
            $this->user_id = $user->id;
            $this->save();
        }
        else {
            // Оператора можно обновить только по ID или обьекту User
            throw new InvalidArgumentException("По параметру ".gettype($user)." невозможно найти оператора!");
        }
    }

    /**
     * Назначить место отправки
     * @param $place
     */
    public function setPlaceFrom($place)
    {
        if (is_integer($place))
        {
            $place = Place::findOrFail($place);
            $this->from_place_id = $place->id;
            $this->save();
        }
        else if (Checker::isValidPlaceObject($place))
        {
            $this->from_place_id = $place->id;
            $this->save();
        }
        else {
            // Место можно обновить только по ID или обьекту Place
            throw new InvalidArgumentException("По параметру ".gettype($place)." невозможно найти место!");
        }
    }

    /**
     * Назначить место назначения (куда идет доставка)
     * @param $place
     */
    public function setPlaceTo($place)
    {
        if (is_integer($place))
        {
            $place = Place::findOrFail($place);
            $this->to_place_id = $place->id;
            $this->save();
        }
        else if (Checker::isValidPlaceObject($place))
        {
            $this->to_place_id = $place->id;
            $this->save();
        }
        else {
            // Место можно обновить только по ID или обьекту Place
            throw new InvalidArgumentException("По параметру ".gettype($place)." невозможно найти место!");
        }
    }

    /**
     * Назначить статус
     * @param int $status
     */
    public function setStatus(int $status)
    {
        if(Checker::isValidOrderStatus($status))
        {
            $this->status = $status;
            $this->save();
        }
        else {
            // Если эта ошибка вызвана, значит
            // необходимо добавить константу с нужным значением в App\Http\Helpers\OrderStatus class
            throw new InvalidArgumentException("Незарегистрированный статус!");
        }
    }

    /**
     * Назначить тариф
     * @param int $tariff
     */
    public function setTariff(int $tariff)
    {
        if(Checker::isValidOrderTariff($tariff))
        {
            $this->tariff = $tariff;
            $this->save();
        }
        else {
            // Если эта ошибка вызвана, значит
            // необходимо добавить константу с нужным значением в App\Http\Helpers\OrderTariff class
            throw new InvalidArgumentException("Незарегистрированный тариф!");
        }
    }

    /**
     * Назначить цену
     * @param $price
     */
    public function setPrice($price)
    {
        if(Checker::isValidPriceForOrder($price))
        {
            $this->price = (float)$price;
            $this->save();
        }
        else {
            // Если эта ошибка вызвана, значит недопустимый тип цены, возможно только int/float
            throw new InvalidArgumentException("Неверный тип для цены!");
        }
    }

    /**
     * Назначить телефон
     * @param $phone
     */
    public function setPhone($phone)
    {
        if(Checker::isValidPhoneForOrder($phone))
        {
            $this->phone = $phone;
            $this->save();
        }
        else {
            // Если эта ошибка вызвана, значит недопустимый тип телефона, возможно только string
            throw new InvalidArgumentException("Неверный тип для телефона!");
        }
    }

    /**
     * Назначить автомобиль
     * @param $car
     */
    public function setCar($car)
    {
        if (is_integer($car))
        {
            $car = Car::findOrFail($car);
            // Если автомобиль не занят ни кем другим
            if($car && !$car->active_driver)
            {
                $this->car_id = $car->id;
                $this->save();
            }
            else {
                throw new LogicException("Автомобиль занят в данный момент другим водителем!");
            }

        }
        else if (Checker::isValidCarObject($car))
        {
            if($car && !$car->active_driver)
            {
                $this->car_id = $car->id;
                $this->save();
            }
            else {
                throw new LogicException("Автомобиль занят в данный момент другим водителем!");
            }
        }
        else {
            // Место можно обновить только по ID или обьекту Car
            throw new InvalidArgumentException("По параметру ".gettype($car)." невозможно найти автомобиль!");
        }
    }

    /**
     * Назначить время старта выполнения заказа
     */
    public function setStartWorkTime()
    {
        $this->start_work_order = Carbon::now();
        $this->save();
    }

    /**
     * Назначить время выполнения заказа (закрытие)
     */
    public function setEndWorkTime()
    {
        $this->end_work_order = Carbon::now();
        $this->save();
    }

    /**
     * Узнать в каком состоянии сейчас заказ (новый/завершен/отклонен)
     * @return string
     */
    public function getCurrentState()
    {
        if( !($this->is_completed && $this->is_cancelled) )
        {
            return 'Новый заказ';
        }
        elseif ($this->is_completed)
        {
            return 'Заказ завершен';
        }
        elseif ($this->is_cancelled)
        {
            return 'Заказ отменен';
        }
    }
}
