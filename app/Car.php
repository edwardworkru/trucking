<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    /**
     * Drivers that can use this car
     */
    public function possible_drivers()
    {
        return $this->belongsToMany('App\Driver', 'car_driver');
    }

    /**
     * Driver that use car just now
     */
    public function active_driver()
    {
        return $this->belongsTo('App\Driver', 'active_driver_id');
    }


}
