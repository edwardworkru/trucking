<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    /**
     * Order
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
