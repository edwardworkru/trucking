<?php

use App\Car;
use App\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::group(['middleware' => 'jwt.auth'], function ($router) {

    Route::group(['prefix' => 'cars'], function () {

        Route::get('/', 'CarController@index');
        Route::get('/{car}', 'CarController@show');

    });

    Route::group(['prefix' => 'drivers'], function () {

        Route::get('/', 'DriverController@index');

    });

    Route::group(['prefix' => 'complaints'], function () {

        Route::get('/', 'ComplaintController@index');

    });

    Route::group(['prefix' => 'orders'], function () {

        Route::get('/', 'OrderController@index');
        Route::get('/{order}', 'OrderController@show');

    });

    Route::get('/statistics', 'InfoController@index');

});

Route::get('/test', function () {
    $order_count_current = Order::where(['is_cancelled' => false, 'is_completed' => false])->count();
    dd($order_count_current);
});
