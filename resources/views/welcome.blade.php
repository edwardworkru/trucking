<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>Taxi</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- + favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/favicon/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('img/favicon/site.webmanifest') }}">
        <link rel="mask-icon" href="{{ asset('img/favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="format-detection" content="telephone=no">
        <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    </head>

    <body>

        <div class="loader-container" style="display: none">
            <div class="loader"></div>
        </div>

        <div id="app">

            <div class="content">
                <div class="flex1">
                    <div class="lkPanel">

                        <div class="logoBg">
                            <router-link :to="{name: 'home'}"><img src="{{ asset('img/logo.svg') }}" alt=""></router-link>
                        </div>

                        <ul class="menuLk">
                            <router-link exact-active-class="active" tag="li" :to="{name: 'orders'}"><a>Заказы</a></router-link>
                            <router-link exact-active-class="active" tag="li" :to="{name: 'drivers'}"><a>Водители</a></router-link>
                            <router-link exact-active-class="active" tag="li" :to="{name: 'photo-controls'}"><a>Фотоконтроль</a></router-link>

                            <router-link exact-active-class="active" tag="li" :to="{name: 'complaints'}"><a>Жалобы</a></router-link>
                            <router-link exact-active-class="active" tag="li" :to="{name: 'map'}"><a>Карта</a></router-link>

                            <login-modal :isactive="isActiveLoginModal"></login-modal>
                            <new-call-modal :isactive="isActiveNewCallModal"></new-call-modal>
                            <new-alert-modal :isactive="isActiveNewAlertModal"></new-alert-modal>
                            <card-driver-modal :isactive="isActiveCardDriverModal"></card-driver-modal>
                            <new-order-modal :isactive="isActiveNewOrderModal"></new-order-modal>
                            <new-complaint-modal :isactive="isActiveNewComplaintModal"></new-complaint-modal>

                        </ul>

                    </div>

                    <div class="lkBox">

                        <header-component></header-component>

                        <router-view></router-view>

                    </div>
                </div>
            </div>

        </div>

        <!-- Jquery 3.3.1 CDN -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="{{ asset('js/script.js') }}"></script>
        <script src="{{ asset('js/script.min.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
