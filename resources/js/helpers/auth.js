export function login(credentials) {
        let payload = {};
        payload.method = 'attemptAuth';
        payload.options = {
            type: 'operator',
            email: credentials.email,
            password: credentials.password
        };

        window.ws.send(JSON.stringify(payload));
}

export function getLocalUser() {
    const userStr = localStorage.getItem("user");

    if(!userStr) {
        return null;
    }

    return JSON.parse(userStr);
}