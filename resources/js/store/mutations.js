export default {
    login(state) {
        state.loading = true;
        state.auth_error = null;
    },
    loginSuccess(state, payload) {
        state.auth_error = null;
        state.isLoggedIn = true;
        state.loading = false;
        state.currentUser = Object.assign({}, payload);

        localStorage.setItem("user", JSON.stringify(state.currentUser))
    },
    loginFailed(state, payload) {
        state.loading = false;
        state.auth_error = payload.error;
    },
    logout(state) {
        localStorage.removeItem("user");
        state.isLoggedIn = false;
        state.currentUser = null;
    },
    setLoading(state, payload) {
        state.loading = payload.flag;
    },
    changeActiveLoginModal(state, payload) {
        state.modals.loginModal.isActive = payload.flag;
    },
    changeActiveNewCallModal(state, payload) {
        state.modals.newCall.isActive = payload.flag;
    },
    changeActiveNewAlertModal(state, payload) {
        state.modals.newAlert.isActive = payload.flag;
    },
    changeActiveCardDriverModal(state, payload) {
        state.modals.cardDriver.isActive = payload.flag;
    },
    /**
     * Example:
     * Child component call mutation with data: {full_name: 'Вася', color: 'red', ...}
     *
     * @param state
     * @param payload
     */
    changeDataForCardDriverModal(state, payload) {
        Object.keys(payload).forEach(item => {
            state.modals.cardDriver.data[item] = payload[item];
        });
    },
    changeActiveNewOrderModal(state, payload) {
        state.modals.newOrder.isActive = payload.flag;
    },
    changeActiveNewComplaintModal(state, payload) {
        state.modals.newComplaint.isActive = payload.flag;
    },

    unsetAllFilters(state) {
        for(let i in state.filters)
        {
            if(typeof state.filters[i] === 'string') state.filters[i] = '';
            if(typeof state.filters[i] === 'boolean') state.filters[i] = false;

            if(typeof state.filters[i] === 'object')
            for(let j in state.filters[i])
            {
                if(typeof state.filters[i][j] === 'string') state.filters[i][j] = '';
                if(typeof state.filters[i][j] === 'boolean') state.filters[i][j] = false;
            }
        }
    },
    setFilterApplied(state, payload) {
        state.isFilterApplied = payload.flag;
    },
    setActiveFilterPerHour(state) {
        state.filters.perHour.isActive = true;
        state.filters.perToday.isActive = false;
        state.filters.perWeek.isActive = false;
        state.filters.period.isActive = false;
        state.filters.period.date_from = '';
        state.filters.period.date_to = '';
    },
    setActiveFilterPerToday(state) {
        state.filters.perHour.isActive = false;
        state.filters.perToday.isActive = true;
        state.filters.perWeek.isActive = false;
        state.filters.period.isActive = false;
        state.filters.period.date_from = '';
        state.filters.period.date_to = '';
    },
    setActiveFilterPerWeek(state) {
        state.filters.perHour.isActive = false;
        state.filters.perToday.isActive = false;
        state.filters.perWeek.isActive = true;
        state.filters.period.isActive = false;
        state.filters.period.date_from = '';
        state.filters.period.date_to = '';
    },
    setActiveFilterPerPeriod(state) {
        state.filters.perHour.isActive = false;
        state.filters.perToday.isActive = false;
        state.filters.perWeek.isActive = false;
        state.filters.period.isActive = true;
        state.filters.period.date_from = '';
        state.filters.period.date_to = '';
    },
    updateFilterByPhone(state, payload) {
        state.filters.phone = payload.phone;
    },
    updateFilterByPeriodDateFrom(state, payload) {
        state.filters.period.date_from = payload.date_from;
    },
    updateFilterByPeriodDateTo(state, payload) {
        state.filters.period.date_to = payload.date_to;
    }
}