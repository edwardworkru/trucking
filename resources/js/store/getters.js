export default {
    isLoading(state) {
        return state.loading;
    },
    isLoggedIn(state) {
        return state.isLoggedIn;
    },
    currentUser(state) {
        return state.currentUser;
    },
    authError(state) {
        return state.auth_error;
    },

    // Modals
    isActiveLoginModal(state) {
        return state.modals.loginModal.isActive;
    },
    isActiveNewCallModal(state) {
        return state.modals.newCall.isActive;
    },
    isActiveNewAlertModal(state) {
        return state.modals.newAlert.isActive;
    },
    isActiveCardDriverModal(state) {
        return state.modals.cardDriver.isActive;
    },
    getDataForCardDriver(state) {
        return state.modals.cardDriver.data;
    },
    isActiveNewOrderModal(state) {
        return state.modals.newOrder.isActive;
    },
    isActiveNewComplaintModal(state) {
        return state.modals.newComplaint.isActive;
    },

    // Filters
    isFilterApplied(state) {
        return state.isFilterApplied;
    },
    isActiveFilterPerHour(state) {
        return state.filters.perHour.isActive;
    },
    isActiveFilterPerToday(state) {
        return state.filters.perToday.isActive;
    },
    isActiveFilterPerWeek(state) {
        return state.filters.perWeek.isActive;
    },
    isActiveFilterPeriod(state) {
        return state.filters.period.isActive;
    },
    getFilterPeriodSettings(state) {
        return state.filters.period;
    },
    getFilterByPhone(state) {
        return state.filters.phone;
    }

}