import {getLocalUser} from "../helpers/auth";
import mutations from "./mutations";
import actions from "./actions";
import getters from "./getters";

const user = getLocalUser();

export default {
    state: {
        currentUser: user,
        isLoggedIn: !! user,
        loading: false,
        auth_error: null,

        modals: {
            loginModal: {
                isActive: false
            },
            newCall: {
                isActive: false
            },
            newAlert: {
                isActive: false
            },
            cardDriver: {
                isActive: false,
                data: {
                    full_name: '',
                    avatar_url: '/img/man.svg',
                    auto_type: '',
                    auto_mark: '',
                    tariff: '',
                    color: '',
                    plate_number: '',
                    additional_info: '',
                    rating: 0.0,
                    day_in_company: 0,
                    hours_online: 0,
                    in_roads: 0,
                    count_success_orders: 0,
                    success_orders_count: 0,
                    created_at: new Date()
                }
            },
            newOrder: {
                isActive: false
            },
            newComplaint: {
                isActive: false
            }
        },

        isFilterApplied: false, // Поле отвечающие за то, применен ли где-либо фильтр
        filters: {
            phone: '',
            nickname: '', // Позывной
            perHour: {
                isActive: false
            },
            perToday: {
                isActive: false
            },
            perWeek: {
                isActive: false
            },
            period: {
                isActive: false,
                date_from: '',
                date_to: ''
            },
            source: {
                site: false,
                application: false,
                dispatcher: false
            }
        }
    },
    getters,
    mutations,
    actions,
    strict: process.env.NODE_ENV !== 'production'
}