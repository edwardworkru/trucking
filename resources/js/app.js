import Vue from 'vue';
import VueRouter from 'vue-router';
import {routes} from './routes';
import storage from './store/index';
import Vuex from 'vuex';
import headerComponent from "./components/header-component";
import loginModal from "./components/modals/login-modal";
import newCallModal from "./components/modals/new-call-modal";
import newAlertModal from "./components/modals/new-alert-modal";
import cardDriverModal from "./components/modals/card-driver-modal";
import newOrderModal from "./components/modals/new-order-modal";
import newComplaintModal from "./components/modals/new-complaint-modal";

Vue.use(VueRouter);
Vue.use(Vuex);

const store = new Vuex.Store(storage);

const router = new VueRouter({
    mode: 'history',
    routes,
});

router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const currentUser = store.state.currentUser;

    if(requiresAuth && !currentUser) {
        next(false);
        store.commit('changeActiveLoginModal', {flag: true});
    } else if (to.path === '/') {
        next("/orders");
    } else {
        next();
    }
});

let app = new Vue({
    el: '#app',
    methods: {
        authOperator() {
            console.log('authOperator', new Date())
        }
    },
    computed: {
        isActiveLoginModal() {
            return this.$store.getters.isActiveLoginModal;
        },
        isActiveNewCallModal() {
            return this.$store.getters.isActiveNewCallModal;
        },
        isActiveNewAlertModal() {
            return this.$store.getters.isActiveNewAlertModal;
        },
        isActiveCardDriverModal() {
            return this.$store.getters.isActiveCardDriverModal;
        },
        isActiveNewOrderModal() {
            return this.$store.getters.isActiveNewOrderModal;
        },
        isActiveNewComplaintModal() {
            return this.$store.getters.isActiveNewComplaintModal;
        }
    },
    created() {
        window.Event = new class {
            constructor() {
                this.vue = new Vue();
            }

            fire(event, data = null) {
                this.vue.$emit(event, data);
            }

            listen(event, callback) {
                this.vue.$on(event, callback);
            }
        };

        const context = this;
        window.ws = new WebSocket('ws://localhost:7001');

        window.ws.onopen = function() {

            console.log("Соединение с WEB-Socket установлено.");

            Event.fire('socket-connection-success');

            Event.listen('callToOperator', function (response) {
                context.$store.commit("changeActiveNewCallModal", {flag: true});
            });

            window.ws.send(JSON.stringify({
                method: 'attemptAuth',
                options: {
                    type: 'operator',
                    email: "test@gmail.com"
                }
            }));
        };

        window.ws.onclose = function(event) {
            window.ws.send(JSON.stringify({
                method: 'unAuthOperator'
            }));
        };

        window.ws.onmessage = response => {
            console.log('RESPONSE', JSON.parse(response.data));
            Event.fire(JSON.parse(response.data).action, JSON.parse(response.data));
        };

        window.ws.onerror = function(error) {
            console.error("Произошла ошибка при работе с WEB-Socket");
        };
    },
    router,
    store,
    components: {
        'header-component': headerComponent,
        'login-modal': loginModal,
        'new-call-modal': newCallModal,
        'new-alert-modal': newAlertModal,
        'card-driver-modal': cardDriverModal,
        'new-order-modal': newOrderModal,
        'new-complaint-modal': newComplaintModal,
    }
});
