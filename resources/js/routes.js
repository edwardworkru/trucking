import Login from './components/auth/Login';
import OrdersPage from './components/pages/OrdersPage';
import DriversPage from './components/pages/DriversPage';
import PhotoControlPage from './components/pages/PhotoControlPage';
import ComplaintsPage from './components/pages/ComplaintsPage';
import MapPage from './components/pages/MapPage';

export const routes = [
    {
        path: '/',
        name: 'home',
        component: OrdersPage,
        meta: {
            requiresAuth: true
        }
    },

    {
        path: '/login',
        name: 'login',
        component: Login
    },

    {
        path: '/orders',
        name: 'orders',
        component: OrdersPage,
        meta: {
            requiresAuth: true
        }
    },

    {
        path: '/drivers',
        name: 'drivers',
        component: DriversPage,
        meta: {
            requiresAuth: true
        }
    },

    {
        path: '/photo-controls',
        name: 'photo-controls',
        component: PhotoControlPage,
    },

    {
        path: '/complaints',
        name: 'complaints',
        component: ComplaintsPage,
        meta: {
            requiresAuth: true
        }
    },

    {
        path: '/map',
        name: 'map',
        component: MapPage,
    },
];
