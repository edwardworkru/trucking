<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Start seeds');
        $this->call(CarsTableSeeder::class);
        $this->call(ComplaintsTableSeeder::class);
        $this->call(DriversTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(PlacesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->command->info('End seeds');
    }
}
