<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('active_driver_id')->nullable(); // ID водителя, который использует автомобиль в данный момент
            $table->string('type')->default('')->comment('Например: пикап, газель и т.д.');
            $table->string('model')->default('')->comment('Например: ГАЗ 3302');
            $table->string('plate_number')->nullable('')->comment('Номерной знак авто');
            $table->string('nickname')->nullable()->comment("Позывной авто, если есть");
            $table->string('additional_info')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
