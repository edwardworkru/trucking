<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->comment('ID оператора, который создал заявку');
            $table->integer('driver_id')->comment('ID водителя, который выполняет/выполнил заказ');
            $table->string('phone')->nullable();
            $table->float('length')->default(0);
            $table->float('expected_time_delivery')->default(0)->comment("Предположительное время в пути");
            //$table->float('time_in_road_delivery')->default(0); // TODO: Нужно ли мне хранить поле "время поездки заказа" у себя в БД?
            $table->float('time_stay')->default(0);
            $table->float('price')->default(0);
            $table->integer('tariff')->default(1);
            $table->integer('status')->default(0);
            $table->integer('is_completed')->default(0)->comment("Завершен заказ или нет");
            $table->integer('is_cancelled')->default(0)->comment("Отменен заказ или нет");
            $table->integer('from_place_id')->comment("ID места, откуда доставляем");
            $table->integer('to_place_id')->comment("ID места, куда доставляем");
            $table->integer('car_id')->comment("ID автомобиля, который выполняет закза");
            $table->string('voice_record')->nullable()->comment("Ссылка на запись разговора");
            $table->timestamp('start_work')->nullable();
            $table->timestamp('start_work_order')->nullable();
            $table->timestamp('end_work_order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
