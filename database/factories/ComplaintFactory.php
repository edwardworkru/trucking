<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Complaint;
use Faker\Generator as Faker;

$factory->define(Complaint::class, function (Faker $faker) {
    return [
        'order_id' => $faker->randomElement([1, 2, 3, 4, 5]),
        'text' => $faker->sentence,
        'phone' => $faker->phoneNumber,
        'other_phone' => $faker->randomElement([$faker->phoneNumber, null])
    ];
});
