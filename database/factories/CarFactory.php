<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Car;
use Faker\Generator as Faker;

$factory->define(Car::class, function (Faker $faker) {
    return [
        'active_driver_id' => rand(1, 2),
        'type' => $faker->randomElement(['Пикап', 'Газель', 'Тент', 'Термос']),
        'model' => $faker->randomElement(['ГАЗ 3022', 'ГАЗ 1058', 'ГАЗ 5547']),
        'plate_number' => $faker->randomElement(['АВ 1020 АВ', 'СМ 1021 СМ', 'ПП 5587 ПП']),
        'additional_info' => $faker->randomElement(['Есть детское кресло', 'Животных не вожу'])
    ];
});
