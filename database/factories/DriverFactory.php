<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Driver;
use Faker\Generator as Faker;

$factory->define(Driver::class, function (Faker $faker) {
    return [
        'full_name' => $faker->firstNameMale.' '.$faker->lastName,
        'phone' => $faker->phoneNumber,
        'additional_info' => $faker->randomElement(['Не курю', 'Не пью', 'Не курю и не пью'])
    ];
});
