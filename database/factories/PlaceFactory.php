<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Place;
use Faker\Generator as Faker;

$factory->define(Place::class, function (Faker $faker) {
    return [
        'latitude' => $faker->randomElement([49.143229980567, 49.323094019433, 28.334295886549, 28.609746113451]),
        'longitude' => $faker->randomElement([49.143229980567, 49.323094019433, 28.334295886549, 28.609746113451]),
        'address' => $faker->country.', '.$faker->city.', '.$faker->streetName.', '.$faker->buildingNumber
    ];
});
