<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'user_id' => rand(1, 2),
        'phone' => $faker->phoneNumber,
        'length' => $faker->randomFloat(null, 0, intval('5000')),
        'expected_time_delivery' => $faker->randomFloat(null, 0, intval('500')),
        'time_stay' => $faker->randomFloat(null, 0, intval('200')),
        'price' => $faker->randomFloat(null, 100, intval('10000')),
        'tariff' => $faker->randomElement([1, 2, 3]),
        'from_place_id' => $faker->randomElement([1, 2, 3, 4, 5]),
        'to_place_id' => $faker->randomElement([1, 2, 3, 4, 5]),
        'status' => $faker->randomElement([1, 2, 3, 4, 5]),
        'car_id' => $faker->randomElement([1, 2, 3, 4, 5]),
        'driver_id' => rand(1, 5),
    ];
});
